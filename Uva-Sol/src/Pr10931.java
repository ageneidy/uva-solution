import java.util.Scanner;
public class Pr10931 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		StringBuilder output;
		while (in.hasNext()) {
			int u = in.nextInt();
			if (u == 0)
				break;
			output = new StringBuilder("");
			int num = 0;
			while (u > 0) {
				if (u % 2 == 1)
					num++;
				output.append(u % 2);
				u /= 2;
			}
			output.reverse();
			System.out.println("The parity of " + output + " is " + num
					+ " (mod 2).");
		}
	}
}
