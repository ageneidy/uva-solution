import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Pr11396 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			int v = in.nextInt();
			if (v == 0)
				break;
			v++;
			int[] color = new int[v];
			Arrays.fill(color, -1);
			ArrayList<Integer>[] G = new ArrayList[v];
			for (int i = 0; i < v; i++)
				G[i] = new ArrayList<Integer>();
			int x, y;
			while ((x = in.nextInt()) != 0 & (y = in.nextInt()) != 0) {
				G[x].add(y);
				G[y].add(x);
			}
			Queue<Integer> q = new LinkedList<Integer>();
			q.add(1);
			color[1] = 0;
			boolean b = true;
			while (!q.isEmpty()) {
				int w = q.poll();
				for (int u : G[w]) {
					if (color[u] == -1) {
						color[u] =1 -  color[w];
						q.add(u);
					}
					if (color[u] == color[w]) {
						b = false;
						break;
					}
				}
			}
			if (b) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}
}
