import java.util.Scanner;

public class Pr371 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			int l = in.nextInt();
			int h = in.nextInt();
			if (l == 0 && h == 0) {
				System.exit(0);
			}
			int c = 0, n = 0;
			for (int j = l; j <= h; j++) {
				int a = j;
				int counter = 0;
				while (a != 1) {
					if (a % 2 == 0) {
						a = a / 2;
					} else {
						a = 3 * a + 1;
					}
					counter += 1;
				}
				if (counter > n) {
					n = counter;
					c = j;
				}
				counter = 0;
			}
			System.out.println("Between " + l + " and " + h + ", " + c
					+ " generates the longest sequence of " + n + " values.");
		}
	}
}