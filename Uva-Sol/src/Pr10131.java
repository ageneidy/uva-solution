import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Pr10131 {
	static ArrayList<pair> a;
	static int[] dp;
	static int[] parent;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		a = new ArrayList<pair>();
		int k = 0 ;
		while (in.hasNext()) {
			a.add(new pair(++k,in.nextInt(), in.nextInt()));
		}
		Collections.sort(a);
		dp = new int[a.size()];
		parent = new int[a.size()];
		for (int i = 0; i < a.size(); i++) {
			dp[i] = 1;
			parent[i] = i;
		}
		for (int i = 0; i < a.size(); i++) {
			int prev = i;
			for (int j = i + 1; j < a.size(); j++)
				if (a.get(prev).weight < a.get(j).weight && a.get(prev).iQ > a.get(j).iQ) {
					if (dp[prev] + 1 > dp[j]) {
						dp[j] = dp[prev] + 1;
						parent[j] = prev;
					}
				}
		}
		int max = dp[0];
		int s = 0;
		for (int i = 1; i < a.size(); i++) {
			if (max < dp[i]) {
				max = dp[i];
				s = i;
			}
		}
		System.out.println(max);
		backtrack(s);
	}

	private static void backtrack(int s) {
		// TODO Auto-generated method stub
		if (s != parent[s])
			backtrack(parent[s]);
		System.out.println(a.get(s).num);
	}

	public static class pair implements Comparable<pair> {
		public int weight;
		public int iQ;
		public int num;
		public pair() {
			weight = 0;
			iQ = 0;
		}

		public pair(int _num ,int _first, int _second) {
			weight = _first;
			iQ = _second;
			num = _num;

		}

		@Override
		public int compareTo(pair o) {
			if (this.weight != o.weight)
				return this.weight - o.weight;
			else
				return this.iQ - o.iQ;
		}

	}
}
