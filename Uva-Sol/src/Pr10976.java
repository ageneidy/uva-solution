import java.util.Scanner;

public class Pr10976 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			double i = in.nextDouble();
			double counter = 0;

			for (double l = i + 1; l <= 2 * i; l++) {
				double x = (l * i) / (l - i);
				if (x == (int) x) {
					counter++;

				}
			}
			System.out.println((long) counter);
			for (double l = i + 1; l <= 2 * i; l++) {
				double x = (double) (l * i) / (l - i);
				if (x == (int) x) {
					System.out.println("1/" + (int) i + " = " + "1/" + (long) x
							+ " + " + "1/" + (long) l);
				}
			}

		}
	}

}
