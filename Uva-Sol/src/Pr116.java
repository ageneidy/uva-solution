import java.util.Scanner;
//simple dp problem
/*
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=52
 * */
public class Pr116 {
	static int M, N;
	static int[][] grid;
	static int[][] memo;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			M = in.nextInt();
			N = in.nextInt();
			grid = new int[M][N];
			memo = new int[M][N];
			for (int i = 0; i < M; i++)
				for (int j = 0; j < N; j++)
					grid[i][j] = in.nextInt();
			for (int i = 0; i < M; i++)
				for (int j = 0; j < N; j++)
					memo[i][j] = -1;
			int min = 999999999;
			int ret, k = 0;
			for (int i = 0; i < M; i++) {
				ret = solve(i, 0);
				if (ret < min) {
					min = ret;
					k = i;
				}
			}
			int i = k;
			int best = i;
			System.out.print(i+1);
			for (int j = 1; j < N; j++) {
				// i think this loop can be modified in a better way , think about it
				if (memo[i][j] < memo[fix(i + 1)][j] ||((i<fix(i+1))&&(memo[i][j] == memo[fix(i + 1)][j]))) {
					if (memo[i][j] < memo[fix(i - 1)][j] ||((i<fix(i-1))&&(memo[i][j] == memo[fix(i - 1)][j]))) {
						best = i;
					} else {
						best = fix(i - 1);
					}
				} else {
					best = fix(i + 1);
					if (memo[fix(i + 1)][j] < memo[fix(i - 1)][j] ||((fix(i+1)<fix(i-1))&&(memo[fix(i+1)][j] == memo[fix(i - 1)][j]))) {
						best = fix(i + 1);
					} else {
						best = fix(i - 1);
					}
				}
				System.out.print(" " + (best+1));
				i = best;
			}
			System.out.println();
			System.out.println(min);
		}
	}

	private static int solve(int i, int j) {
		// TODO Auto-generated method stub
		if (j == N)
			return 0;
		if (memo[i][j] != -1)
			return memo[i][j];
		return memo[i][j] = grid[i][j]
				+ Math.min(Math.min(solve(fix(i + 1), j + 1), solve(i, j + 1)),
						solve(fix(i - 1), j + 1));
	}

	private static int fix(int i) {
		// TODO Auto-generated method stub
		return (i % M + M) % M;
	}
}
