import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr10116 {
	static int[][] len;
	static char[][] g;
	static int row, col;;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String[] a = bf.readLine().split(" ");
			row = Integer.parseInt(a[0]);
			col = Integer.parseInt(a[1]);
			int position = Integer.parseInt(a[2]);
			if (row == 0 && col == 0 && position == 0)
				break;
			g = new char[row][col];
			len = new int[row][col];
			for (int i = 0; i < row; i++) {
				g[i] = bf.readLine().toCharArray();
				for (int u = 0; u < col; u++)
					len[i][u] = -1;
			}
			visit(0, position - 1, 0);
		}
	}

	private static void visit(int row1, int col1, int num) {
		// TODO Auto-generated method stub
		if (row1 < 0 || row1 >= row || col1 < 0 || col1 >= col) {
			System.out.println(num + " step(s) to exit");
			return;
		}
		if (len[row1][col1] == -1) {
			len[row1][col1] = num;
		} else {
			System.out.println((len[row1][col1]) + " step(s) before a loop of "
					+ (num - (len[row1][col1])) + " step(s)");
			return;
		}
		switch (g[row1][col1]) {
		case 'N':
			visit(row1 - 1, col1, 1 + num);
			break;
		case 'S':
			visit(row1 + 1, col1, num + 1);
			break;
		case 'E':
			visit(row1, col1 + 1, num + 1);
			break;
		case 'W':
			visit(row1, col1 - 1, num + 1);
			break;
		}

	}
}
