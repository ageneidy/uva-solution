import java.util.Scanner;
/*
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=&problem=977
 * */
public class Pr10036 {
	static int N, K;
	static int[] num;
	static int[][] memo;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		while (tc-- > 0) {
			N = in.nextInt();
			K = in.nextInt();
			num = new int[N];
			memo = new int[N][K];
			for (int i = 0; i < N; i++)
				for (int j = 0; j < K; j++)
					memo[i][j] = -1;

			for (int i = 0; i < N; i++) {
				num[i] = in.nextInt();
			}
			if (cost(0, 0) == 1)
				System.out.println("Divisible");
			else
				System.out.println("Not divisible");
		}
	}

	public static int cost(int i, int j) {
		// TODO Auto-generated method stub
		if (i == N)
			if (j == 0) {
				return  1;
			} else {
				return 0;
			}
		if (memo[i][j] != -1)
			return memo[i][j];
		if (i == N) {
			if (j == 0)
				return memo[i][j] = 1;
			return memo[i][j] = 0;
		}
		if (cost(i + 1, fix(j + num[i])) == 1
				|| cost(i + 1, fix(j - num[i])) == 1)
			return memo[i][j] = 1;
		return memo[i][j] = 0;
	}

	public static int fix(int i) {
		return (i % K + K) % K;
	}
}
