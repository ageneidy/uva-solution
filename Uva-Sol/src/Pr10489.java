import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr10489 {
	public static void main(String[] args) throws NumberFormatException,
			IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int tc = Integer.parseInt(bf.readLine());
		String line = "";
		String[] q;
		while (tc-- > 0) {
			line = bf.readLine();
			q = line.split("\\s+");
			int f = Integer.parseInt(q[0]);
			int b = Integer.parseInt(q[1]);
			int num = 0;
			while (b-- > 0) {
				line = bf.readLine();
				q = line.split("\\s+");
				int reminder = 1;
				for (int i = 1; i < q.length; i++) {
					reminder = reminder * Integer.parseInt(q[i])% f;
				}
				num = (num+reminder)%f;
			}
			System.out.println(num);
		}
	}
}
