import java.math.BigInteger;
import java.util.Scanner;

public class Pr10494 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			BigInteger q = in.nextBigInteger();
			String t = in.next();
			BigInteger w = in.nextBigInteger();
			if (t.equals("/")) {
				System.out.println(q.divide(w));
			} else if (t.equals("%")) {
				System.out.println(q.mod(w));
			}
		}
	}
}
