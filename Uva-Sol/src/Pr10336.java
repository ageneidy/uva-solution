import java.util.ArrayList;
import java.util.Scanner;

public class Pr10336 {
	public boolean[][] visited;
	public char[][] grid;
	public ArrayList<Integer> g;
	public ArrayList<Character> h;

	public void dfs(char s, int x, int y) {
		if (!(x >= 0 && y >= 0 && x < grid.length && y < grid[0].length))
			return;
		if ((grid[x][y] != s) || visited[x][y])
			return;
		visited[x][y] = true;
		dfs(s, x + 1, y);
		dfs(s, x, y + 1);
		dfs(s, x, y - 1);
		dfs(s, x - 1, y);

	}

	public void run() {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		for (int y = 1; y <= tc; y++) {
			int q = in.nextInt();
			int w = in.nextInt();
			visited = new boolean[q][w];
			grid = new char[q][w];
			in.nextLine();
			for (int i = 0; i < q; i++) {
				grid[i] = in.nextLine().toCharArray();
			}
			g = new ArrayList<Integer>();
			h = new ArrayList<Character>();
			for (int i = 0; i < q; i++) {
				for (int j = 0; j < w; j++) {
					if (!visited[i][j]) {
						dfs(grid[i][j], i, j);
						add(grid[i][j]);
					}
				}
			}
			sort();
			System.out.println("World #" + y);
			for (int i = 0; i < g.size(); i++) {
				System.out.println(h.get(i) + ": " + g.get(i));
			}
		}
	}

	private void sort() {
		int d = g.size();
		for (int i = 0; i < d; i++)
			for (int j = i + 1; j < d; j++) {
				if (g.get(j) > g.get(i)) {
					int e = g.get(j);
					g.set(j, g.get(i));
					g.set(i, e);
					char w = h.get(j);
					h.set(j, h.get(i));
					h.set(i, w);
				}
			}
		for (int i = 0; i < d; i++)
			for (int j = i + 1; j < d; j++) {
				if ((g.get(j) == g.get(i)) && h.get(j) < h.get(i)) {
					int e = g.get(j);
					g.set(j, g.get(i));
					g.set(i, e);
					char w = h.get(j);
					h.set(j, h.get(i));
					h.set(i, w);
				}
			}

	}

	private void add(char c) {
		boolean b = true;
		for (int i = 0; i < g.size(); i++) {
			if (h.get(i) == c) {
				g.set(i, 1 + g.get(i));
				b = false;
			}
		}
		if (b) {
			g.add(1);
			h.add(c);
		}

	}

	public static void main(String[] args) {
		new Pr10336().run();
	}

}
