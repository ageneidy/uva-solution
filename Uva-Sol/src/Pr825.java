import java.awt.Checkbox;
import java.util.Arrays;
import java.util.Scanner;
//simple dp
public class Pr825 {
	static boolean[][] blocked;
	static int N, K;
	static int[][] memo;

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();
		boolean first = true;
		while (q-- > 0) {

			N = in.nextInt();
			K = in.nextInt();
			memo = new int[N + 1][K + 1];
			blocked = new boolean[N + 1][K + 1];
			in.nextLine();
			for (int i = 0; i <= N; i++)
				Arrays.fill(memo[i], -1);
			// whenever there is a passage block the cell in-order not to pass 
			for (int i = 1; i <= N; i++) {
				String u = in.nextLine();
				String[] a = u.split(" ");
				for (int j = 1; j < a.length; j++) {
					blocked[i][Integer.parseInt(a[j])] = true;
				}
			}

			if (!first)
				System.out.println();
			first = false;
			memo[N][K] = 1;
			System.out.println(Check(1, 1));
		}
	}

	private static int Check(int i, int j) {
		// TODO Auto-generated method stub
		if (!valid(i, j))
			return 0;
		if (memo[i][j] != -1)
			return memo[i][j];
		return (memo[i][j] = Check(i + 1, j) + Check(i, j + 1));

	}

	private static boolean valid(int i, int j) {
		// TODO Auto-generated method stub
		if (i > N || j > K || blocked[i][j])
			return false;
		return true;
	}
}
