import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Pr11060 {
	static boolean[] visited;
	static int len, cases;
	static String[] mat;
	static String[][] relations;
	static StringBuilder sol;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = 0;
		String m = "";

		while (in.hasNextInt()) {
			sol = new StringBuilder();
			len = in.nextInt();
			visited = new boolean[len];
			mat = new String[len];
			for (int i = 0; i < len; i++) {
				mat[i] = in.next();
			}
			sol.append("Case #" + (++tc)
					+ ": Dilbert should drink beverages in this order:");
			cases = in.nextInt();
			relations = new String[cases][2];
			for (int i = 0; i < cases; i++) {
				relations[i][0] = in.next();
				relations[i][1] = in.next();
			}
			for (int i = 0; i < len; i++) {
				if (!visited[i])
					dfs(i);
			}
			sol.append(".\n\n");
			System.out.print(sol);
		}

	}

	private static void dfs(int i) {
		// TODO Auto-generated method stub
		if (visited[i])
			return;
		visited[i] = true;
		for (int q = 0; q < cases; q++) {
			if (relations[q][1].equals(mat[i]))
				dfs(getNum(relations[q][0]));
		}
		sol.append(" " + mat[i]);
	}

	private static int getNum(String string) {
		// TODO Auto-generated method stub
		for (int j = 0; j < len; j++) {
			if (string.equals(mat[j]))
				return j;
		}
		return 0;
	}
}
