import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Pr11902 {
	static ArrayList<Integer>[] G;
	static boolean[] visited, connected;
	static int nodes, prevent;
	static StringBuilder line, output;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int tc = Integer.parseInt(bf.readLine());
		for (int t = 1; t <= tc; t++) {
			output = new StringBuilder("");
			nodes = Integer.parseInt(bf.readLine());
			G = new ArrayList[nodes];
			visited = new boolean[nodes];
			connected = new boolean[nodes];
			prevent = -1;
			for (int i = 0; i < nodes; i++)
				G[i] = new ArrayList<Integer>();
			for (int i = 0; i < nodes; i++) {
				String[] u = bf.readLine().split(" ");
				for (int j = 0; j < u.length; j++) {
					if (u[j].equals("1"))
						G[i].add(j);
				}
			}
			output.append("Case " + t + ":\n");
			print();
			output.append(line);
			check();
			System.out.print(output);
		}
	}

	private static void check() {
		// TODO Auto-generated method stub
		checkConnected();
		for (int i = 0; i < nodes; i++) {
			Arrays.fill(visited, false);
			prevent = i;
			dfs(0);
			printSol();
			output.append(line);
		}
	}

	private static void checkConnected() {
		// TODO Auto-generated method stub
		Arrays.fill(visited, false);
		dfs(0);
		for (int i = 0; i < nodes; i++) {
			connected[i] = visited[i];
		}

	}

	private static void dfs(int node) {
		if (node == prevent)
			return;
		visited[node] = true;

		for (int i = 0; i < G[node].size(); i++) {
			int next = G[node].get(i);
			if (!visited[next])
				dfs(next);
		}
	}

	private static void print() {
		// TODO Auto-generated method stub
		line = new StringBuilder("");
		line.append("+");
		for (int i = 0; i < 2 * nodes - 1; i++)
			line.append("-");
		line.append("+");
		line.append("\n");
	}

	private static void printSol() {
		// TODO Auto-generated method stub
		output.append("|");
		for (int i = 0; i < nodes; i++)
			if (!connected[i] || visited[i])
				output.append("N|");
			else
				output.append("Y|");

		output.append("\n");
	}
}