import java.util.Arrays;
import java.util.Scanner;

public class Pr10066 {
	static int[] T1;
	static int[] T2;
	static int[][] cache;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean first = false;
		int w = 0;
		while (true) {
			int u = in.nextInt();
			int q = in.nextInt();
			if (u == 0 && q == 0) {
				break;
			}
			T1 = new int[u];
			T2 = new int[q];
			cache = new int[u][q];
			for (int i = 0; i < u; i++)
				Arrays.fill(cache[i], -1);
			for (int i = 0; i < u; i++)
				T1[i] = in.nextInt();
			for (int i = 0; i < q; i++)
				T2[i] = in.nextInt();
			System.out.println("Twin Towers #" + (++w));
			System.out.println("Number of Tiles : " + solve(u-1, q-1));
			System.out.println();

		}
	}

	private static int solve(int i, int j) {
		// TODO Auto-generated method stub
		if (i < 0 || j < 0)
			return 0;
		if (cache[i][j] != -1)
			return cache[i][j];
		if (T1[i] == T2[j])
			return cache[i][j] = 1 + solve(i - 1, j - 1);
		return cache[i][j] = Math.max(solve(i - 1, j), solve(i, j - 1));
	}
}