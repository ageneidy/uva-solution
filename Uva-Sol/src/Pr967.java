import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Pr967 {
	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int[] sol = { 113, 131, 197, 199, 311, 337, 373, 719, 733, 919, 971,
				991, 1193, 1931, 3119, 3779, 7793, 7937, 9311, 9377, 11939,
				19391, 19937, 37199, 39119, 71993, 91193, 93719, 93911, 99371,
				193939, 199933, 319993, 331999, 391939, 393919, 919393, 933199,
				939193, 939391, 993319, 999331 };
		int[] circular = new int[1000000];
		Arrays.fill(circular, 0);
		for (int i = 0; i < sol.length; i++)
			circular[sol[i]] = 1;
		for (int i = 1; i < 1000000; i++)
			circular[i] += circular[i - 1];
		int N, M;
		while (true) {
			String line = bf.readLine();
			String[] u = line.split(" ");
			N = Integer.parseInt(u[0]);
			if (N == -1)
				break;
			M = Integer.parseInt(u[1]);
			int k = circular[M] - circular[N-1];
			if (k == 0)
				System.out.println("No Circular Primes.");
			else if (k == 1) {
				System.out.println("1 Circular Prime.");
			} else {
				System.out.println(k + " Circular Primes.");
			}
		}
	}
}
// private static int solve() {
// // TODO Auto-generated method stub
// int rank = 0;
// if (N % 2 == 0)
// N++;
// for (int i = N; i < M; i += 2) {
// if (circular[i]||solution(i))
// rank++;
// }
// return rank;
// }

// private static boolean solution(int b) {
// // TODO Auto-generated method stub
// int usage = b;
// int k = (int) (Math.log10(b));
// int mod = (int) Math.pow(10, k);
// int[] pack = new int[k + 1];
// pack[0] = b;
// for (int w = 1; w <= k; w++) {
// pack[w] = usage = (usage % mod) * 10 + (int) usage / mod;
// if (!isPrime[usage])
// return false;
// }
// for (int i = 0; i < pack.length; i++)
// circular[pack[i]] = true;
// return true;
// }
//
// private static void seives(int w) {
// // TODO Auto-generated method stub
// isPrime = new boolean[w + 1];
// Arrays.fill(isPrime, true);
// isPrime[0] = false;
// isPrime[1] = false;
// for (int i = 4; i <= w; i += 2)
// isPrime[i] = false;
// for (int i = 3; i <= 1000; i += 2)
// if (isPrime[i])
// for (double q = i * i; q <= w; q += i)
// isPrime[(int) q] = false;
// }
// }
