import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Pr168 {
	static ArrayList<Integer>[] G;
	static boolean[] prevent;
	static long k, counter;
	static StringBuilder outPut;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String u = bf.readLine();
			if (u.equals("#"))
				break;
			if (u.length() == 0)
				continue;
			outPut = new StringBuilder("");
			counter = 0;
			String[] a = u.split(" ");
			k = Integer.valueOf(a[3]);
			conStructGraph(a[0]);
			play(a[2].charAt(0) - 'A', a[1].charAt(0) - 'A');
		}
	}

	private static void play(int from, int to) {
		// TODO Auto-generated method stub
		counter++;
		if (counter == k) {
			counter = 0;
			prevent[to] = true;
			if (outPut.length() == 0)
				outPut.append((char) (to + 'A'));
			else
				outPut.append(" " + (char) (to + 'A'));

		}
		boolean b = true;
		for (int i = 0; i < G[to].size(); i++) {
			if (G[to].get(i) != from && G[to].get(i) != to
					&& !prevent[G[to].get(i)]) {
				b = false;
				play(to, G[to].get(i));
				break;
			}
		}
		if (b) {
			if (outPut.lastIndexOf((char) (to + 'A') + "") == outPut.length() - 1
					&& outPut.length() >= 1) {
				outPut.deleteCharAt(outPut.length() - 1);
			}
			if (outPut.length() == 0)
				System.out.println("/" + (char) (to + 'A'));
			else if (outPut.lastIndexOf(" ") == outPut.length() - 1)
				System.out.println(outPut + "/" + (char) (to + 'A'));
			else
				System.out.println(outPut + " /" + (char) (to + 'A'));
		}
	}

	private static void conStructGraph(String u) {
		// TODO Auto-generated method stub
		creatLists();
		String[] q = u.split(";");
		for (int i = 0; i < q.length; i++) {
			int j = q[i].charAt(0) - 'A';
			for (int t = 2; t < q[i].length(); t++) {
				if (q[i].charAt(t) == '.')
					continue;
				G[j].add(q[i].charAt(t) - 'A');
			}
		}
	}

	private static void creatLists() {
		// TODO Auto-generated method stub
		G = new ArrayList[300];
		prevent = new boolean[300];
		for (int i = 0; i < 300; i++)
			G[i] = new ArrayList<Integer>();
	}
}
