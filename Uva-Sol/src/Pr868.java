import java.util.Scanner;

public class Pr868 {
	public static String start=" ";
	public static String end;

	static void premutation(int x, int y, int xposition, int yposition,
			int[][] grid, int i,int max) {
		if (max == i) {
			i=0;
			max++;
		}
		if (xposition - 1 > 0 && grid[xposition - 1][yposition] == i) {
			premutation(x, y, xposition - 1, yposition, grid, i+1,max);
		}
		if (yposition - 1 > 0 && grid[xposition][yposition - 1] == i) {
			premutation(x, y, xposition, yposition - 1, grid, i+1,max);
		}
		if (xposition + 1 < x && grid[xposition + 1][yposition] == i) {
			premutation(x, y, xposition + 1, yposition, grid, i+1,max);
		}
		if (yposition + 1 < y && grid[xposition][yposition + 1] == i) {
			premutation(x, y, xposition, yposition + 1, grid, i + 1,max);
		}
		if(yposition==y-1){
			System.out.println(start);
			System.out.println(xposition+ " " + yposition);
		}
		
		return;

	}

	static void prem(int x, int y, int[][] grid) {
		for (int i = 0; i < y; i++) {
			if (grid[0][i] == 1) {
				start = String.valueOf(i)+" " + String.valueOf(0);
				premutation(x, y, 0, i, grid, 1,1);
			}
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int u = in.nextInt();
		for (int q = 0; q < u; q++) {
			int x = in.nextInt();
			int y = in.nextInt();
			int[][] grid = new int[x][y];
			for (int w = 0; w < x; w++) {
				for (int e = 0; e < y; e++) {
					grid[w][e] = in.nextInt();
				}
			}
			prem(x, y, grid);

		}
	}
}
