import java.util.Arrays;
import java.util.Scanner;
// simple knapsack problem
public class Pr10130 {
	static int[] weight;
	static int[] price;
	static int num;
	static int [][] memo;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		while (tc-- > 0) {
			num = in.nextInt();
			weight = new int[num];
			price = new int[num];
			memo = new int[num][31];
			for(int i = 0 ; i < num ; i++)
				Arrays.fill(memo[i], -1);
			for (int i = 0; i < num; i++) {
				price[i] = in.nextInt();
				weight[i] = in.nextInt();
			}
			int persons = in.nextInt();
			int overall = 0;
			for(int i = 0 ; i <persons ; i++){
				overall+= solve(0,in.nextInt());
			}
			System.out.println(overall);
		}
	}

	private static int solve(int i, int reminder) {
		// TODO Auto-generated method stub
		if(i==num)return 0;
		if(memo[i][reminder]!=-1)return memo[i][reminder];
		int choice1 = solve(i+1, reminder);
		int choice2 = 0;
		if(reminder>= weight[i])
			choice2 =  solve(i+1, reminder-weight[i])+price[i];
		return memo[i][reminder] = Math.max(choice1, choice2);
	}
}
