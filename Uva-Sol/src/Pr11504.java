import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Pr11504 {
	static ArrayList<Integer>[] a;
	static int connected;
	static int currentRoot;
	static boolean[] visited;
	static boolean[] root;
	static ArrayList<Integer> w;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		while (tc-- > 0) {
			connected = 0;
			w = new ArrayList<Integer>();
			int nodes = in.nextInt();
			a = new ArrayList[nodes + 1];
			visited = new boolean[nodes + 1];
			root = new boolean[nodes + 1];
			Arrays.fill(visited, false);
			Arrays.fill(root, false);
			for (int i = 0; i < nodes + 1; i++)
				a[i] = new ArrayList<Integer>();
			int edges = in.nextInt();
			for (int i = 0; i < edges; i++)
				a[in.nextInt()].add(in.nextInt());

			for (int i = 1; i <= nodes; i++) {
				if (!visited[i]) {
					dfs(i);
				}
			}
			Arrays.fill(visited, false);
			int ans = 0;
			int s = 0;
			for (int i = w.size() - 1; i >= 0; i--) {
				s = w.get(i);
				if (!visited[s]) {
					dfs1(s);
					ans++;
				}
			}
			System.out.println(ans);
		}
	}

	private static void dfs(int u) {
		// TODO Auto-generated method stub
		visited[u] = true;
		for (int i : a[u])
			if (!visited[i])
				dfs(i);
		w.add(u);
	}

	private static void dfs1(int u) {
		// TODO Auto-generated method stub
		visited[u] = true;
		for (int i : a[u])
			if (!visited[i])
				dfs1(i);

	}
}
