import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Pr124 {
	static String[] p;
	static String[][] q;
	static int size;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line, constraints;
		boolean blank = false;
		while ((line = bf.readLine()) != null) {
			constraints = bf.readLine();
			if (blank)
				System.out.println();
			blank = true;
			p = line.split(" ");
			Arrays.sort(p);
			size = p.length;
			StringTokenizer tokens = new StringTokenizer(constraints, " ");
			q = new String[constraints.split(" ").length / 2][2];
			boolean[] visited = new boolean[size];
			Arrays.fill(visited, false);
			int i = 0;
			while (tokens.hasMoreTokens()) {
				q[i][0] = tokens.nextToken();
				q[i++][1] = tokens.nextToken();
			}
			StringBuilder output = new StringBuilder("");
			backTrack(visited, output);
		}
	}

	private static void backTrack(boolean[] visited, StringBuilder output) {
		// TODO Auto-generated method stub
		if (output.length() == size) {
			// System.out.println("a77at");
			System.out.println(output);
			return;
		}
		for (int i = 0; i < size; i++) {
			if (visited[i])
				continue;
			if (checkPriority(visited, i))
				continue;
			// System.out.println("a77a");
			output.append(p[i]);
			visited[i] = true;
			backTrack(visited, output);
			visited[i] = false;
			output.deleteCharAt(output.length() - 1);
		}
	}

	private static boolean checkPriority(boolean[] visited, int i) {
		// TODO Auto-generated method stub
		for (int j = 0; j < q.length; j++)
			if (q[j][0].equals(p[i]) && isvisited(q[j][1], visited))
				return true;
		return false;
	}

	private static boolean isvisited(String string, boolean[] visited) {
		// TODO Auto-generated method stub
		int i;
		for (i = 0; i < size; i++) {
			if (string.equals(p[i]))
				break;
		}
		return visited[i];
	}
}
