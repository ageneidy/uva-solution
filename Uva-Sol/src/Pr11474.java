import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
//MST reverse
public class Pr11474 {
	static int[] sets;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			int n = in.nextInt();
			int e = in.nextInt();
			if (n == 0 && e == 0)
				break;
			ArrayList<Pair> graph = new ArrayList<Pair>();
			for (int i = 0; i < e; i++) {
				int q = in.nextInt();
				int r = in.nextInt();
				int w = in.nextInt();
				graph.add(Pair.makePair(q, r, w));
			}
			Collections.sort(graph);
			sets = new int[n + 1];
			for (int i = 0; i <= n; i++)
				sets[i] = i;
			ArrayList<Integer> q = getCosts(graph);
			if (q.isEmpty())
				System.out.println("forest");
			else {
				System.out.print(q.get(0));
				for (int i = 1; i < q.size(); i++)
					System.out.print(" " + q.get(i));
				System.out.println();
			}
		}
	}
	private static ArrayList<Integer> getCosts(ArrayList<Pair> graph) {
		// TODO Auto-generated method stub
		ArrayList<Integer> q = new ArrayList<Integer>();
		q.clear();
		while (!graph.isEmpty()) {
			Pair currnt = graph.get(0);
			graph.remove(0);
			if (sets[currnt.node_1] == sets[currnt.node_2])
				q.add(currnt.weight);
			else
				jusitfy(currnt.node_1, currnt.node_2);

		}
		Collections.sort(q);
		return q;
	}

	private static void jusitfy(int node_1, int node_12) {
		// TODO Auto-generated method stub
		int q = sets[node_12];
		for (int i = 0; i < sets.length; i++)
			if (sets[i] == q)
				sets[i] = sets[node_1];
	}

	public static class Pair implements Comparable<Pair> {
		public int node_1;
		public int node_2;
		public int weight;

		public Pair() {
			// TODO Auto-generated constructor stub
			node_1 = 0;
			node_2 = 0;
			weight = 0;

		}

		public Pair(int node1, int node2, int weight) {
			// TODO Auto-generated constructor stub
			node_1 = node1;
			node_2 = node2;
			this.weight = weight;

		}

		private static Pair makePair(int node1, int node2, int weight) {
			// TODO Auto-generated method stub
			return new Pair(node1, node2, weight);
		}

		@Override
		public int compareTo(Pair o) {
			// TODO Auto-generated method stub
			return (weight - o.weight);
		}

	}
}
