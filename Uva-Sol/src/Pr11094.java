import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr11094 {
	static char[][] g;
	static boolean[][] visited;
	static int recursion, row, col;
	static char o;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line;
		String[] u;

		while ((line = bf.readLine()) != null) {
			u = line.split(" ");
			row = Integer.parseInt(u[0]);
			col = Integer.parseInt(u[1]);
			g = new char[row][col];
			visited = new boolean[row][col];
			for (int i = 0; i < row; i++) {
				g[i] = bf.readLine().toCharArray();
			}
			u = bf.readLine().split(" ");
			int currentRow = Integer.parseInt(u[0]);
			int currentcol = Integer.parseInt(u[1]);
			recursion = 0;
			o = g[currentRow][currentcol];
			checkRegion(currentRow, currentcol);
			int max = 0;
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					if (g[i][j] == o && !visited[i][j]) {
						recursion = 0;
						checkRegion(i, j);
						if (recursion > max)
							max = recursion;
					}
				}
			}
			System.out.println(max);
			bf.readLine();

		}
	}

	private static void checkRegion(int i, int j) {
		// TODO Auto-generated method stub
		j = (j + col) % col;
		if (i >= row || i < 0 || visited[i][j] || g[i][j] != o)
			return;
		recursion++;
		visited[i][j] = true;
		checkRegion(i + 1, j);
		checkRegion(i, j + 1);
		checkRegion(i - 1, j);
		checkRegion(i, j - 1);

	}
}
