import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr11110 {
	static int size;
	static int[][] g;
	static boolean[][] visited;
	static int[] dx = { 0, 0, 1, -1 };
	static int[] dy = { 1, -1, 0, 0 };
	static boolean[] good;
	static boolean valid;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while (!(line = bf.readLine()).equals("0")) {
			size = Integer.parseInt(line);
			g = new int[size][size];
			good = new boolean[size];
			visited = new boolean[size][size];
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					g[i][j] = -1;
			String[] q;
			valid = true;
			String p ;
			for (int i = 1; i < size; i++) {
				p=bf.readLine();
				q = p.split(" ");
				if (q.length < 2 * size) {
					valid = false;
				}else{
					construct(q, i);
				}
			}
			if (!valid) {
				System.out.println("wrong");
				continue;
			}
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					if (g[i][j] == -1)
						g[i][j] = size;
			boolean c = true;
			for (int i = 0; i < size && c; i++)
				for (int j = 0; j < size && c; j++)
					if (!visited[i][j]) {
						if (!good[g[i][j] - 1]) {
							good[g[i][j] - 1] = true;
							backTrack(i, j, g[i][j]);
						} else {
							c = false;
						}
					}
			if (!c) {
				System.out.println("wrong");
				continue;
			}
			System.out.println("good");
			// for (int i = 0; i < size; i++) {
			// for (int j = 0; j < size; j++) {
			// System.out.printf("%2d ",g[i][j]);
			// }
			// System.out.println();
			// }
		}
	}

	private static void backTrack(int i, int j, int k) {
		// TODO Auto-generated method stub
		if (i < 0 || j < 0 || i >= size || j >= size || g[i][j] != k
				|| visited[i][j])
			return;
		visited[i][j] = true;
		for (int q = 0; q < 4; q++) {
			backTrack(i + dx[q], j + dy[q], k);
		}

	}

	private static void construct(String[] charArray, int q) {
		// TODO Auto-generated method stub
		int num = 0;
		int u, j;
		for (int i = 0; i < charArray.length-1; i += 2) {
			if(charArray[i].equals(""))continue;
			u = Integer.parseInt(charArray[i]) - 1;
			j = Integer.parseInt(charArray[i + 1]) - 1;
			if(g[u][j]==-1){
				num++;
			g[u][j] = q;
			}
		}
		if(num!=size)valid=false;
	}
}
