import java.text.DecimalFormat;
import java.util.Scanner;
// simple dp
public class Pr11341 {
	static int M, N;
	static int [][] grade;
	static int [][] memo;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		while (tc-- > 0) {
			N = in.nextInt();
			M = in.nextInt();
			grade = new int[N][M];
			memo = new int[N+1][M+1];
			for(int i = 0 ; i < N ; i++)
				for(int j = 0 ; j < M ; j++)
					grade[i][j]=in.nextInt();
			for(int i = 0 ; i < N+1 ; i++)
				for(int j = 0 ; j < M+1 ; j++)
					memo[i][j]=-1;
			double a = (double)solve(0,0,M)/N;
			if(a > 0)
				//tricky to round
				System.out.printf("Maximal possible average mark - %.2f.\n", a);
			else
				System.out.println("Peter, you shouldn\'t have played billiard that much.");

		}
	}
	// we may work without memo
	//
	private static int solve(int i, int j,int left) {
		// TODO Auto-generated method stub
		while(j<M && i < N && grade[i][j]<5)j++;
		if(left >=0 && i == N ){
			return 0;
		}
		if(j>=M||i>=N||left<0)return -99999999;
		if(memo[i][left]!=-1)return memo[i][left];
		return memo[i][left] = Math.max(grade[i][j] + solve(i+1, 0, left-(j+1)),solve(i, j+1, left)) ;
	}
}
