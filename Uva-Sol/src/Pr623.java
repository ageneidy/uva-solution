import java.math.BigInteger;
import java.util.Scanner;

public class Pr623 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			int u = in.nextInt();
			BigInteger k = BigInteger.valueOf(1);
			for (long i = u; i > 0; i--) {
				k = k.multiply(BigInteger.valueOf(i));
			}
			System.out.println(u + "!");
			System.out.println(k);
		}
	}

}
