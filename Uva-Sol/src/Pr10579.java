import java.math.BigInteger;
import java.util.Scanner;
public class Pr10579 {
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		while(in.hasNext()){
		int u = in.nextInt();
		BigInteger sum = BigInteger.valueOf(0);
		BigInteger fib1 = BigInteger.valueOf(1);
		BigInteger fib2 = BigInteger.valueOf(1);
		if (u==1){
			System.out.println(fib1);
		}else if (u==2){
			System.out.println(fib2);
		}else{
		for (int i = 3 ; i <=u ; i++){
			sum = fib2.add(fib1);
			fib1 = fib2 ;
			fib2 = sum ;
		}
		System.out.println(sum);
	}
	}
	}

}
