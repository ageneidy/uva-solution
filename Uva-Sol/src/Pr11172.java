import java.util.Scanner;

public class Pr11172 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		byte t = in.nextByte();
		if (t >= 15) {
			System.out.println("Wrong input");
			System.exit(0);
		}
		
		while (t > 0) {
			t--;
			int a = in.nextInt();
			int b = in.nextInt();

			if (a > b) {
				System.out.println(">");
			} else if (a < b) {
				System.out.println("<");
			} else {
				System.out.println("=");
			}
		}
	}
}
