import java.util.Arrays;
import java.util.Scanner;

public class Pr562 {
	static int[] coins;
	static int[][] memo;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		while (tc-- > 0) {
			int u = in.nextInt();
			coins = new int[u];
			memo = new int[u][50001];
			for (int i = 0; i < u; i++)
				Arrays.fill(memo[i], -1);

			for (int i = 0; i < u; i++)
				coins[i] = in.nextInt();
			System.out.println(solve(0, 0));
		}
	}

	private static int solve(int i, int reminder) {
		// TODO Auto-generated method stub
		if (i == coins.length)
			return reminder;
		if (memo[i][reminder] != -1)
			return memo[i][reminder];
		return memo[i][reminder] = Math.min(
				solve(i + 1, Math.abs(reminder - coins[i])),
				solve(i + 1, Math.abs(reminder + coins[i])));
	}
}
