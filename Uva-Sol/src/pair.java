public class pair implements Comparable<pair> {
	public int first;
	public int second;

	public pair() {
		first = 0;
		second = 0;
	}

	public pair(int _first, int _second) {
		first = _first;
		second = _second;

	}

	@Override
	public int compareTo(pair o) {
		if (this.first != o.first)
			return this.first - o.first;
		else
			return this.second - o.second;
	}

}