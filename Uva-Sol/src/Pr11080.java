import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Pr11080 {
	static int nodes, roads;
	static ArrayList<Integer>[] graph;
	static boolean[] visited;
	static int[] bipart;
	static boolean valid;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int tc = in.nextInt();
		int x, y;
		while (tc-- > 0) {
			nodes = in.nextInt();
			roads = in.nextInt();
			visited = new boolean[nodes];
			graph = new ArrayList[nodes];
			bipart = new int[nodes];
			Arrays.fill(bipart, -1);
			for (int i = 0; i < nodes; i++)
				graph[i] = new ArrayList<Integer>();
			for (int i = 0; i < roads; i++) {
				x = in.nextInt();
				y = in.nextInt();
				graph[x].add(y);
				graph[y].add(x);
			}
			int[] cnt = new int[2];
			int numOfGaurds = 0;
			valid = true;
			for (int i = 0; i < nodes && valid; i++) {
				cnt[0]=0;
				cnt[1]=0;
				if (bipart[i] == -1) {
					dfs(i, cnt, 0);
					numOfGaurds += Math.max(1, Math.min(cnt[0], cnt[1]));
				}
			}
			if (valid)
				System.out.println(numOfGaurds);
			else
				System.out.println(-1);
		}
	}

	private static void dfs(int i, int[] cnt, int flag) {
		// TODO Auto-generated method stub
		if (bipart[i] == flag)
			return;
		if (bipart[i] == -1) {
			bipart[i] = flag;
			cnt[flag]++;
		}
		if (bipart[i] != flag) {
			valid = false;
		}
		if (!valid)
			return;
		for (int u : graph[i]) {
			dfs(u, cnt, 1 - flag);
		}
	}
}
