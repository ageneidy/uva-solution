import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
//MST
public class Pr908 {
	static int[] sets;
	static int num;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean blank = false;
		while (in.hasNext()) {
			num = in.nextInt();
			ArrayList<Pair> G = new ArrayList<Pair>();
			ArrayList<Pair> G_updated = new ArrayList<Pair>();
			sets = new int[num + 1];
			for (int i = 1; i < num; i++) {
				int q = in.nextInt();
				int w = in.nextInt();
				int e = in.nextInt();
				// G.add(Pair.makePair(q, w, e));
			}
			int k = in.nextInt();
			for (int i = 0; i < k; i++) {
				int q = in.nextInt();
				int w = in.nextInt();
				int e = in.nextInt();
				G.add(Pair.makePair(q, w, e));
			}
			int m = in.nextInt();
			for (int i = 0; i < m; i++) {
				int q = in.nextInt();
				int w = in.nextInt();
				int e = in.nextInt();
				G_updated.add(Pair.makePair(q, w, e));
				G.add(Pair.makePair(q, w, e));
			}
			Collections.sort(G_updated);
			Collections.sort(G);
			if(blank)System.out.println();
			blank=true;
			for (int i = 1; i <= num; i++)
				sets[i] = i;
			System.out.println(getcost(G_updated));
			for (int i = 1; i <= num; i++)
				sets[i] = i;
			System.out.println(getcost(G));
		}
	}

	private static int getcost(ArrayList<Pair> Graph) {
		// TODO Auto-generated method stub
		int cost = 0;
		while (!Graph.isEmpty()) {
			Pair q = Graph.get(0);
			Graph.remove(0);
			if (sets[q.node_1] != sets[q.node_2]) {
				makeEqual(q.node_1, q.node_2);
				cost += q.weight;
			}
		}
		return cost;
	}

	private static void makeEqual(int node_1, int node_12) {
		// TODO Auto-generated method stub
		int q = sets[node_12];
		for (int i = 1; i <= num; i++)
			if (sets[i] == q)
				sets[i] = sets[node_1];
	}

	public static class Pair implements Comparable<Pair> {
		public int weight;
		public int node_1;
		public int node_2;

		public Pair() {
			// TODO Auto-generated constructor stub
			weight = 0;
			node_1 = 0;
			node_2 = 0;
		}

		public Pair(int node_1, int node_2, int weight) {
			// TODO Auto-generated constructor stub
			this.node_1 = node_1;
			this.node_2 = node_2;
			this.weight = weight;
		}

		private static Pair makePair(int node_1, int node_2, int weight) {
			// TODO Auto-generated method stub
			return new Pair(node_1, node_2, weight);
		}

		@Override
		public int compareTo(Pair o) {
			// TODO Auto-generated method stub
			return (weight - o.weight);
		}

	}
}
