import java.util.ArrayList;
import java.util.Scanner;

public class Pr352 {
	public boolean[][] visited;
	public char[][] grid;
	public ArrayList<Integer> g;
	public ArrayList<Character> h;

	public void dfs(int x, int y) {
		if (!(x >= 0 && y >= 0 && x < grid.length && y < grid[0].length))
			return;
		if ((grid[x][y] != '1') || visited[x][y])
			return;

		visited[x][y] = true;
		dfs(x + 1, y);
		dfs(x, y + 1);

		dfs(x, y - 1);
		dfs(x - 1, y);

		dfs(x + 1, y + 1);
		dfs(x - 1, y - 1);

		dfs(x - 1, y + 1);
		dfs(x + 1, y - 1);

	}

	public void run() {
		Scanner in = new Scanner(System.in);
		for (int y = 1; in.hasNext(); y++) {
			int q = in.nextInt();
			visited = new boolean[q][q];
			grid = new char[q][q];
			in.nextLine();
			for (int i = 0; i < q; i++) {
				grid[i] = in.nextLine().toCharArray();
			}
			int counter = 0;
			for (int i = 0; i < q; i++) {
				for (int j = 0; j < q; j++) {
					if (!visited[i][j] && (grid[i][j] == '1')) {
						dfs(i, j);
						counter++;
					}
				}
			}
			System.out.println("Image number " + y + " contains " + counter
					+ " war eagles.");
		}
	}

	public static void main(String[] args) {
		new Pr352().run();
	}

}
