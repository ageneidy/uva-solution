import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
//problem in path
public class Pr_10131_2 {
	static int[] path;
	static ArrayList<pair> a;
	static int[] memo;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int j = 1;
		a = new ArrayList<pair>();
		while (in.hasNext()) {
			a.add(new pair(j++, in.nextInt(), in.nextInt()));
		}
		Collections.sort(a);
		memo = new int[a.size()];
		path = new int[a.size()];
		for (int i = 0; i < a.size(); i++)
			path[i] = i;
			Arrays.fill(memo, -1);
		int w = 0;
		int  s = 0;
		for (int i = 0; i < a.size(); i++) {
			int q = lis(i, 0) + 1;
			if (w < q){
				w = q;
				s = i;
			}
		}

		System.out.println(w);
		int max = path[0];
		int u = 0;
		for (int i = 1; i < path.length; i++) {
			if (path[i] > max) {
				max = path[i];
				u = i;
			}
		}
		backtrack(u);
	}
	private static void backtrack(int s) {
		// TODO Auto-generated method stub
		if (s != path[s])
			backtrack(path[s]);
		System.out.println(a.get(s).num);
	}
	private static int lis(int prev, int next) {
		// TODO Auto-generated method stub
		if (prev >= memo.length || next >= memo.length)
			return 0;
		if (memo[prev] != -1){
			return memo[prev];
		}
		int w = 0;
		int q = lis(prev, next + 1);
		int s = 0;
		if (a.get(prev).iQ > a.get(next).iQ
				&& a.get(prev).weight < a.get(next).weight)
			s = lis(next, next + 1) + 1;
		if (s > w) {
			w = s;
			path[next] = prev;
		}
		if (q > w){
			w = q;
		}
		return memo[prev] = w;
	}

	public static class pair implements Comparable<pair> {
		public int num;
		public int weight;
		public int iQ;

		public pair() {
			num = 0;
			weight = 0;
			iQ = 0;
		}

		public pair(int _num, int _first, int _second) {
			num = _num;
			weight = _first;
			iQ = _second;

		}

		@Override
		public int compareTo(pair o) {
			if (this.weight != o.weight)
				return this.weight - o.weight;
			else
				return o.iQ - this.iQ;
		}

	}
}