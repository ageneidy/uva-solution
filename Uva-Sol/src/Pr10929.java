import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Pr10929 {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringBuffer output = new StringBuffer("");
		String line = "";
		while (true) {
			line = bf.readLine();
			if ("0".equals(line))
				break;
			BigInteger t = new BigInteger(line);
			if (t.remainder(BigInteger.valueOf(11)).compareTo(BigInteger.ZERO) == 0) {
				output.append(line).append(" is a multiple of 11.\n");
			} else {
				output.append(line).append(" is not a multiple of 11.\n");
			}
		}
		System.out.print(output);
	}
}