import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr11953 {
	static char[][] g;
	static int size;

	public static void main(String[] args) throws NumberFormatException,
			IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(bf.readLine());
		int num;
		for (int q = 1; q <= t; q++) {
			num = 0;
			size = Integer.parseInt(bf.readLine());
			g = new char[size][size];
			for (int j = 0; j < size; j++)
				g[j] = bf.readLine().toCharArray();
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					if (g[i][j] == 'x') {
						recursion(i, j);
						num++;
					}
				}
			}
			System.out.println("Case " + q + ": " + num);

		}
	}

	private static void recursion(int i, int j) {
		// TODO Auto-generated method stub
		if (i < 0 || j < 0 || i >= size || j >= size || g[i][j] == '.')
			return;
		g[i][j] = '.';
		recursion(i + 1, j);
		recursion(i, j + 1);
		recursion(i - 1, j);
		recursion(i, j - 1);
	}
}
