import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
//c is the accepted
public class Pr10484 {
	public static class factor {
		public factor(int u, int num2) {
			// TODO Auto-generated constructor stub
			factor = u;
			num = num2;
		}

		int factor;
		int num;
	}

	static ArrayList<Integer> primes;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		seives(5000);
		while (in.hasNext()) {
			int n = in.nextInt();
			int m = Math.abs(in.nextInt());
			if (n == 0 && m == 0)
				return;
			System.out.printf("%d\n",solve(n, m));
		}
	}

	private static int solve(int n, int m) {
		// TODO Auto-generated method stub
		
		if(n==0){
			if(m==1)return 1;
			return 0;
		}
		ArrayList<factor> factors = new ArrayList<factor>();
		int k = m;
		int u = 0;
		int[] vis = new int[n+1];
		Arrays.fill(vis, 1);
		for (int i = 0; i <= n; i++) {
			u = primes.get(i);
			if (u > n)
				break;
			vis[i] += getnum(n, u);
			while (k % u == 0) {
				k /= u;
				vis[i]--;
			}
		}
		int sum = 1;
		for (int i : vis)
			sum *= i;
		if (sum <= 0||k>1)
			return 0;
		return sum;
	}

	private static int getnum(int n, int factor) {
		// TODO Auto-generated method stub
		int rep = 0;
		for (long i = factor; i <= n; i *= factor)
			rep += n / i;
		return rep;
	}

	private static void seives(int w) {
		// TODO Auto-generated method stub
		boolean[] sei = new boolean[w + 1];
		Arrays.fill(sei, true);
		primes = new ArrayList<Integer>();
		sei[0] = false;
		sei[1] = false;
		for (int i = 4; i <= w; i += 2)
			sei[i] = false;
		for (int i = 3; i <= w; i += 2)
			if (sei[i])
				for (int q = i * i; q <= w; q += i)
					sei[q] = false;
		for (int i = 2; i <= w; i++) {
			if (sei[i])
				primes.add(i);
		}
	}
}
/*
 * 
 * #include<stdio.h>
int t[100];
int main()
{
    int i,j,k,m,n,d;
    while(1)
    {
	scanf("%d %d",&n,&d);
	if (d<0)
	    d=-d;
	if (n==0&&d==0)
	    break;
	if (n==0)
	{
	    if (d==1)
		printf("1\n");
	    else
		printf("0\n");
	    continue;
	}
	for (i=0;i<=n;i++)
	    t[i]=0;
	for (i=2;i<=n;i++)
	{
	    k=i;
	    for (j=2;j<=n;j++)
	    {
		while (k%j==0)
		{
		    t[j]++;
		    k/=j;
		}
	    }
	    while(d%i==0)
	    {
		t[i]--;
		d/=i;
	    }
	}
	k=0;
	long long l=1;
	for (i=2;i<=n;i++)
	{
	    l*=t[i]+1;
	    if (t[i]<0)
		k=1;
	}
	if (d>1||k==1)
	{
	    printf("0\n");
	    continue;
	}
	printf("%lld\n",l);
    }
    return 0;
}
 * */
