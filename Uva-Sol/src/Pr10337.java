import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr10337 {
	static int[][] memo;
	static int[][] grid;
	static int len;

	public static void main(String[] args) throws NumberFormatException,
			IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int tc = Integer.parseInt(bf.readLine());
		String line;
		while (tc-- > 0) {
			bf.readLine();
			len = Integer.parseInt(bf.readLine()) / 100;
			grid = new int[10][len];
			memo = new int[10][len];
			//reversed
			for (int i = 9; i >= 0; i--) {
				line = bf.readLine();
				line = line.trim();
				String[] tokens = line.split("\\s+");
				for (int j = 0; j < tokens.length; j++)
					grid[i][j] = Integer.parseInt(tokens[j]);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < len; j++)
					memo[i][j] = -1;
			}
			System.out.println(solve(0, 0));
			System.out.println("");
		}
		
	}

	public static int solve(int i, int j) {
		// TODO Auto-generated method stub
		//this is a corner test case
		if(j == len&&i==0)return 0;
		if (!valid(i, j))
			return 99999999;
		if (memo[i][j] != -1)
			return memo[i][j];
		//this is a corner test case u can not fly in zero lvl
		if(i==0&&j!=len-1)
			return (memo[i][j] = -1* grid[i][j]+
					Math.min(20 + solve(i - 1, j + 1), 60 + solve(i + 1, j + 1)));
		
		
		
		return (memo[i][j] = -1* grid[i][j]+ Math.min(30 + solve(i, j + 1),
				Math.min(20 + solve(i - 1, j + 1), 60 + solve(i + 1, j + 1))));
	}

	public static boolean valid(int i, int j) {
		// TODO Auto-generated method stub
		if ((i < 0 || i >= 10 || j == len))
			return false;
		return true;
	}
}
