import java.util.Scanner;

public class Pr10299 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int j = 0;

		while (true) {
			int num = 0;
			j = in.nextInt();
			if (j == 0)
				break;
			System.out.println(fi(j));
		}
	}

	static int fi(int n) {
		int result = n;
		if(n==1)return 0;
		if (n % 2 == 0) {
			while (n % 2 == 0)
				n /= 2;
			result -= result / 2;
		}

		for (int i = 3; i * i <= n; i+=2) {
			if (n % i == 0)
				result -= result / i;
			while (n % i == 0)
				n /= i;
		}
		if (n > 1)
			result -= result / n;
		return result;
	}
}
