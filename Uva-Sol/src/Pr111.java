import java.util.Scanner;

public class Pr111 {
	static int[] correct;
	static int[] check;
	static int N;
	static int [][] cache;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		N = in.nextInt();
		correct = new int[N];
		int [] p = new int [N];
		for (int i = 0; i < N; i++) {
			p[i] = in.nextInt();
		}
		for(int i = 0 ; i < N ; i++){
			correct[p[i]-1]=i;
		}
		while (in.hasNext()) {
			check = new int[N];
			cache= new int[N][N];
			for(int i = 0 ; i < N ; i++)
				for(int j = 0 ; j < N ; j++)
					cache[i][j] = -1;
			p = new int[N];
			for(int i = 0 ; i < N ; i++)
				p[i] = in.nextInt();
			for(int i = 0 ; i < N ; i++){
				check[p[i]-1]=i;
			}
			int max = 0 ;
			int u = 0 ;
			for(int i = 0 ; i < N ; i++){
				u = (LCS(0,i));
				if(u>max)max = u;
			}
			System.out.println(max);
		}
	}

	private static int LCS(int i, int j) {
		// TODO Auto-generated method stub
		if(i >= N ||j >= N)return  0;
		if(cache[i][j] !=-1)return cache[i][j] ;
		if(correct[i]==check[j])return cache[i][j] =1+LCS(i+1, j+1);
		return cache[i][j] = Math.max(LCS(i, j+1), LCS(i+1, j));
	}
}
