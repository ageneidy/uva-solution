import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;
 
public class Pr429 {
    static LinkedList<Integer>[] adjList;
    static TreeMap<Integer, Integer> dist;
    static Queue<Integer> q;
 
    static boolean isAdj(String a, String b) {
        int counter = 0;
        if (a.length() != b.length())
            return false;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) != b.charAt(i))
                counter++;
            if (counter > 1)
                return false;
        }
        return true;
    }
 
    static int getIndex(String[] inp, String s) {
        for (int i = 0; i < inp.length; i++)
            if (inp[i].equals(s))
                return i;
        return -1;
    }
 
    static int bfs(int st, int end) {
        dist.put(st, 0);
        q.add(st);
        while (!q.isEmpty()) {
            int u = q.poll();
            for (int i = 0; i < adjList[u].size(); i++) {
                int v = adjList[u].get(i);
                if (!dist.containsKey(v)) {
                    if (v == end)
                        return dist.get(u) + 1;
                    dist.put(v, dist.get(u) + 1);
                    q.add(v);
                }
            }
        }
        return 0;
    }
 
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder out = new StringBuilder();
        int t = Integer.parseInt(br.readLine());
        String[] inp, sp;
        String l;
        int counter;
        q = new LinkedList<Integer>();
        br.readLine();
        for (int i = 0; i < t; i++) {
            inp = new String[200];
            counter = 0;
            l = br.readLine();
            while (!l.equals("*")) {
                inp[counter++] = l;
                l = br.readLine();
            }
            adjList = new LinkedList[counter];
            for (int j = 0; j < counter; j++)
                adjList[j] = new LinkedList<Integer>();
            for (int j = 0; j < counter; j++)
                for (int j2 = j + 1; j2 < counter; j2++)
                    if (isAdj(inp[j], inp[j2])) {
                        adjList[j].add(j2);
                        adjList[j2].add(j);
                    }
            l = br.readLine();
            while (!l.equals("")) {
                dist = new TreeMap<Integer, Integer>();
                q.clear();
                sp = l.split(" ");
                int start = getIndex(inp, sp[0]);
                int end = getIndex(inp, sp[1]);
                out.append(l + " " + bfs(start, end) + "\n");
                l = br.readLine();
                if (l == null)
                    break;
            }
            if (i != t - 1)
                out.append("\n");
        }
        System.out.print(out);
    }
}