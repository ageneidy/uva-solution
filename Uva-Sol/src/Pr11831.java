import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pr11831 {
	static int pos, xAxis, yAxis, sticker, row, col;
	static char[][] map;
	static int[] dx = { 0, 1, 0, -1 };
	static int[] dy = { -1, 0, 1, 0 };

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			pos = -1;
			xAxis = -1;
			yAxis = -1;
			sticker = 0;
			String q = bf.readLine();
			String[] s = q.split(" ");
			if (s[0].equals("0") && s[1].equals("0") && s[2].equals("0"))
				break;
			row = Integer.valueOf(Integer.valueOf(s[0]));
			col = Integer.valueOf(Integer.valueOf(s[1]));
			int cases = Integer.valueOf(Integer.valueOf(s[2]));
			map = new char[row][col];
			for (int i = 0; i < row; i++)
				map[i] = bf.readLine().toCharArray();

			char[] u = bf.readLine().toCharArray();// instructions
			boolean b = true;
			for (int i = 0; b && i < row; i++)
				for (int j = 0; b && j < col; j++)
					switch (map[i][j]) {
					case 'N':// North
						pos = 0;
						b = false;
						xAxis = j;
						yAxis = i;
						break;
					case 'L':// East
						pos = 1;
						xAxis = j;
						yAxis = i;
						b = false;
						break;
					case 'S':// South
						pos = 2;
						b = false;
						xAxis = j;
						yAxis = i;
						break;
					case 'O':// West
						pos = 3;
						xAxis = j;
						yAxis = i;
						b = false;
						break;
					}

			for (int i = 0; i < cases; i++) {
				switch (u[i]) {
				case 'F':
					move();
					break;
				case 'D':
					pos = (pos + 1) % 4;
					break;
				case 'E':
					pos = (((pos - 1) % 4) + 4) % 4;
					break;
				}
			}
			System.out.println(sticker);
		}
	}

	private static void move() {
		// TODO Auto-generated method stub
		if (xAxis + dx[pos] >= 0 && xAxis + dx[pos] < col
				&& yAxis + dy[pos] < row && yAxis + dy[pos] >= 0) {
			// System.out.println((xAxis+dx[pos])+" "+(yAxis+dy[pos]));
			// System.out.println(map[(xAxis + dx[pos])][(yAxis + dy[pos])]);
			switch (map[(yAxis + dy[pos])][(xAxis + dx[pos])]) {
			case '#':// by5osh hna 3ady
				return;
			case '*':// elmoshkela hna ya fandem mesh by5osh hna 5ales
				sticker++;
				map[(yAxis + dy[pos])][(xAxis + dx[pos])] = '.';
				xAxis += dx[pos];
				yAxis += dy[pos];
				return;
			default:// by5osh hna 3ady
				xAxis += dx[pos];
				yAxis += dy[pos];
				return;
			}
		}
	}
}
