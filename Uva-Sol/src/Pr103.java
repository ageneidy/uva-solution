import java.util.Arrays;
import java.util.Scanner;

public class Pr103 {
	static int[] dp;
	static int[] parent;
	static int[][] a;
	static int k, n;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			k = in.nextInt();
			n = in.nextInt();
			a = new int[k][n];
			dp = new int[k];
			parent = new int[k];
			for (int i = 0; i < k; i++)
				for (int j = 0; j < n; j++)
					a[i][j] = in.nextInt();
			for (int i = 0; i < k; i++)
				Arrays.sort(a[i]);
			for (int i = 0; i < k; i++) {
				dp[i] = 1;
				parent[i] = i;
			}
			for(int q= 0 ; q < k ;q++)
			for (int i = 0; i < k; i++) {
				int prev = i;
				for (int j = 0; j <k; j++)
					if (nested(j, i)) {
						if (dp[prev] + 1 > dp[j]) {
							dp[j] = dp[prev] + 1;
							parent[j] = prev;
						}
					}
			}
			
			int max = dp[0];
			int s = 0;
			for (int i = 1; i < dp.length; i++) {
				if (dp[i] > max) {
					max = dp[i];
					s = i;
				}
			}
			System.out.println(max);
			if(max>1)
			backTrack(parent[s]);
			System.out.println((s + 1));

		}
	}

	private static void backTrack(int s) {
		// TODO Auto-generated method stub
		if (parent[s] != s)
			backTrack(parent[s]);
		System.out.print((s + 1) + " ");
	}

	private static boolean nested(int i, int j) {
		// TODO Auto-generated method stub
		for (int q = 0; q < n; q++)
			if (a[i][q] <= a[j][q])
				return false;
		return true;
	}
}
