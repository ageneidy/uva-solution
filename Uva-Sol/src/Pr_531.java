import java.util.Scanner;

public class Pr_531 {
	static String[] q;
	static String[] q2;
	static String[][] memo;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean first = false;
		while (in.hasNext()) {
			String u = "";
			while (true) {
				String line = in.nextLine() + " ";
				if (line.contains("#"))
					break;
				u = u + line;
			}
			q = u.trim().split("\\s+");
			u = "";
			while (true) {
				String line = in.nextLine() + " ";
				if (line.contains("#"))
					break;
				u = u + line;
			}
			q2 = u.trim().split("\\s+");
			memo = new String[q.length][q2.length];
			System.out.println(solve(0, 0).trim());
		}
	}

	private static String solve(int i, int j) {
		// TODO Auto-generated method stub

		if (i >= q.length || j >= q2.length)
			return " ";
		if (memo[i][j] != null)
			return memo[i][j];
		if (q[i].equals(q2[j]))
			return memo[i][j] = q[i] + " " + solve(i + 1, j + 1);
		String u = solve(i + 1, j);
		String k = solve(i, 1 + j);

		return memo[i][j] = ((u.length() > k.length()) ? u : k);
	}
}
