import java.util.Arrays;
import java.util.Scanner;

// goo.gl/chgySQ
public class Pr10003 {
	static int[][] memo;
	static int len;
	static int[] cuts;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (true) {
			len = in.nextInt();
			if (len == 0)
				break;
			int u = in.nextInt();
			memo = new int[u + 2][u + 2];
			for (int i = 0; i < u + 2; i++)
				Arrays.fill(memo[i], -1);
			cuts = new int[u + 2];
			cuts[0] = 0;
			cuts[u + 1] = len;
			for (int i = 1; i <= u; i++)
				cuts[i] = in.nextInt();
			Arrays.sort(cuts);
			System.out.println("The minimum cutting is " + solve(0, u + 1)
					+ ".");
		}
	}

	private static int solve(int i, int j) {
		// TODO Auto-generated method stub
		if (j - i <= 1)
			return 0;
		if (memo[i][j] != -1)
			return memo[i][j];
		memo[i][j] = Integer.MAX_VALUE;
		for (int q = i + 1; q < j; q++) {
			memo[i][j] = Math.min(memo[i][j], solve(i, q) + solve(q, j)
					+ (cuts[j] - cuts[i]));
		}
		return memo[i][j];
	}
}
