import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Pr_315 {
	private static ArrayList<Integer>[] g;
	private static int counter, artCounter;
	private static int[] dfs_num, dfs_low, parent;
	private final static int DFS_white = -1;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line;
		int source = 0, to = 0;
		String[] tokens;
		while (true) {
			line = bf.readLine();
			int u = Integer.parseInt(line.split(" ")[0]);
			if (u == 0)
				break;
			dfs_low = new int[u + 1];
			dfs_num = new int[u + 1];
			parent = new int[u + 1];
			Arrays.fill(dfs_num, DFS_white);
			Arrays.fill(dfs_low, 0);
			Arrays.fill(parent, 0);
			g = new ArrayList[u + 1];
			artCounter = 0;
			counter = 0;
			for (int i = 1; i <= u; i++)
				g[i] = new ArrayList<Integer>();
			while (true) {
				line = bf.readLine();
				tokens = line.split(" ");
				source = Integer.parseInt(tokens[0]);
				if (source == 0 && tokens.length == 1)
					break;
				for (int i = 1; i < tokens.length; i++) {
					to = Integer.parseInt(tokens[i]);
					g[to].add(source);
					g[source].add(to);
				}
			}
			for (int i = 1; i <= u; i++) {
				if (dfs_num[i] == DFS_white) {
					getArticulationPoint(i);
				}
			}
			System.out.println(artCounter);
		}
	}

	private static void getArticulationPoint(int u) {
		// TODO Auto-generated method stub
		dfs_low[u] = dfs_num[u] = counter++;
		for (int v : g[u]) {
			if (dfs_num[v] == DFS_white) {
				parent[v] = u;
				getArticulationPoint(v);
				if (dfs_low[v] >= dfs_num[u])
					artCounter++;
				dfs_low[u]= Math.min(dfs_low[u], dfs_low[v]);
			} else {
				if (v != parent[u])
					dfs_low[u] = Math.min(dfs_low[u], dfs_num[v]);
			}
		}
	}
}
