import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Pr_10776 {
	static String input,last;
	static int len, wordLength;

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while ((line = bf.readLine()) != null) {
			String[] o = line.split(" ");
			char [] a = o[0].toCharArray();
			Arrays.sort(a);
			input = new String(a);
			last="";
			len = Integer.valueOf(o[1]);
			wordLength = input.length();
			StringBuilder outPut = new StringBuilder("");
			combination(outPut, 0);
		}
	}

	private static void combination(StringBuilder outPut, int i) {
		// TODO Auto-generated method stub
		if (outPut.length() == len) {
			if(!outPut.toString().equals(last)){
				last=outPut.toString();
				System.out.println(last);
			}
			return;
		}
		for (int k = i; k < wordLength; k++) {
			outPut.append(input.charAt(k));
			combination(outPut, k+1);
			outPut.deleteCharAt(outPut.length() - 1);
		}

	}
}
