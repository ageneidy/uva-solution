import java.util.Scanner;

public class Pr10420 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int g = in.nextInt();
		String[] list = new String[g];
		int[] listnum = new int[g];
		int counter = 0;
		in.nextLine();
		boolean b = true;
		for (int i = 0; i < g; i++) {
			b = true;
			String q = in.nextLine();
			int k = 0;
			String[] p = q.split(" ");
			String u = p[0];
			if (counter == 0) {
				list[0] = u;
				listnum[0] = 1;
				++counter;
				continue;
			} else {
				for (int z = 0; z < counter; z++) {
					if (list[z].compareTo(u) == 0) {
						listnum[z]++;
						b = false;
						break;
					}
				}
				if (b) {
					list[counter] = u;
					listnum[counter] = 1;
					counter++;

				}
			}
		}
		boolean w = true;
		for (int i = 0; w && i < counter; i++) {
			w = false;
			for (int j = 0; j < counter - 1; j++) {
				if (list[j].compareTo(list[j + 1]) > 0) {
					String k = list[j + 1];
					list[j + 1] = list[j];
					list[j] = k;
					int q = listnum[j + 1];
					listnum[j + 1] = listnum[j];
					listnum[j] = q;
					w = true;
				}

			}
		}

		for (int i = 0; i < counter; i++) {
			System.out.println(list[i] + " " + listnum[i]);
		}
	}

}
