import java.util.Scanner;

public class Pr494 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			char [] u = in.nextLine().toCharArray();
			int count = 0;
			boolean b = false;
			for(char q : u){
				if(Character.isLetter(q)){
					if(!b){
						count++;
						b = true;
					}
				}else{
					b= false;
				}
			}
			System.out.println(count);
		}
	}
}
