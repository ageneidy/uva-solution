import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
 
public class Pr10305 {
        static ArrayList<Integer>[] priority;
        static int size, cases;
        static boolean[] visited;
        static ArrayList<Integer> sol;
 
        public static void main(String[] args) throws IOException {
                Scanner in = new Scanner(System.in);
                while (true) {
                        size = in.nextInt();
                        cases = in.nextInt();
                        if(size == 0 && cases == 0)break;
                        priority = new ArrayList[size + 1];
                        for (int i = 0; i <= size; i++)
                                priority[i] = new ArrayList<Integer>();
                        for (int i = 0; i < cases; i++) {
                                int f = in.nextInt();
                                int t = in.nextInt();
                                priority[t].add(f);
                        }
                        sol = new ArrayList<Integer>();
                        visited = new boolean[size + 1];
                        for (int i = 1; i <= size; i++)
                                if (!visited[i])
                                        dfs(i);
                        System.out.print(sol.get(0));
                        for (int i = 1; i < sol.size(); i++)
                                System.out.print(" " + sol.get(i));
                        System.out.println();
                }
        }
 
        public static void dfs(int i) {
                if (visited[i])
                        return;
                visited[i] = true;
                for (int q = 0; q < priority[i].size(); q++) {
                        dfs(priority[i].get(q));
                }
                sol.add(i);
        }
 
}