import java.io.*;
import java.util.*;

public class Pr624 {
	int n, m, currntSum;
	ArrayList<Integer> printed;

	void run() {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			n = sc.nextInt();
			m = sc.nextInt();
			ArrayList<Integer> g = new ArrayList<Integer>();
			ArrayList<Integer> output = new ArrayList<Integer>();
			printed = new ArrayList<Integer>();
			for (int i = 0; i < m; i++) {
				g.add(sc.nextInt());
			}
			currntSum = 0;
			combination(g, output, 0);
			for (int i : printed)
				System.out.print(i + " ");
			System.out.println("sum:" + currntSum);
		}

		sc.close();
	}

	int sum(ArrayList<Integer> output) {
		int p = 0;
		for (int u : output)
			p += u;
		return p;
	}

	private void combination(ArrayList<Integer> g, ArrayList<Integer> output,
			int q) {

		if (q > m) {
			return;
		}
		int sum = sum(output);
		if (sum > n)
			return;
		if (sum > currntSum) {
			currntSum = sum;
			printed = new ArrayList<Integer>();
			for (int i : output)
				printed.add(i);
		}

		for (int i = q; i < m; i++) {
			output.add(g.get(i));
//			for (int w : output)
//				System.out.print(w + " ");
//			System.out.println("sum:" + sum(output));
			combination(g, output, i + 1);
			output.remove(output.size() - 1);
		}
	}

	public static void main(String[] args) {
		new Pr624().run();
	}
}
