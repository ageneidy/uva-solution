import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Pr200 {

	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		String line;
		ArrayList<String> input = new ArrayList<String>();
		while (!(line = bf.readLine()).equals("#"))
			input.add(line);
		int size = input.size();
		char[][] a = new char[size][];
		for (int i = 0; i < size; i++)
			a[i] = input.get(i).toCharArray();
		int max = -1;
		for (int i = 0; i < size; i++)
			if (a[i].length > max)
				max = a[i].length;
		ArrayList<Character> chars = new ArrayList<Character>();
		ArrayList<Character> sol = new ArrayList<Character>();
		for (int i = 0; i < size; i++)
			for (int j = 0; j < a[i].length; j++) {
				if (!chars.contains(a[i][j]))
					chars.add(a[i][j]);
			}
		for (int i = 0; i < max; i++)
			for (int j = 0; j < a.length; j++) {
				if (a[j].length <= i)
					continue;
				for (int t = j + 1; t < a.length; t++) {
					if (a[t].length <= i)
						continue;
					if (equality(a, j, t, i)) {
						if (!sol.contains(a[j][i]))
							sol.add(a[j][i]);
						if (!sol.contains(a[t][i]))
							sol.add(a[t][i]);
					}
				}
			}
		for (int i = 0; i < sol.size(); i++)
			System.out.print(sol.get(i));
		System.out.println();
	}

	private static boolean equality(char[][] a, int j, int t, int i) {
		// TODO Auto-generated method stub
		if ((t - j) > 1)
			return false;
		for (int q = 0; q < i; q++) {
			if (a[j][q] != a[t][q])
				return false;
		}
		return true;
	}
}
