import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Pr10139 {
	public static class factor {
		public factor(int u, int num2) {
			// TODO Auto-generated constructor stub
			num = num2;
			prime = u;
		}

		int num, prime;
	}

	static boolean[] seive;
	static final int maxx = 50000;
	static ArrayList<Integer> a;
	static ArrayList<factor> factors;

	static void seives() {
		a = new ArrayList<Integer>();
		seive = new boolean[maxx + 1];
		Arrays.fill(seive, true);
		seive[0] = false;
		seive[1] = false;
		for (int i = 4; i <= maxx; i += 2)
			seive[i] = false;
		for (int i = 3; i <= maxx; i += 2)
			if (seive[i])
				for (int j = 2 * i; j <= maxx; j += i)
					seive[j] = false;
		a.add(2);
		for (int i = 3; i <= maxx; i += 2)
			if (seive[i])
				a.add(i);
	}

	public static void main(String[] args) {
		seives();
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			factors = new ArrayList<factor>();
			int n = in.nextInt();
			int m = in.nextInt();
			if (solve(n, m))
				System.out.println(m + " divides " + n + "!");
			else
				System.out.println(m + " does not divide " + n + "!");
		}
	}

	static int getnum(int n, int p) { // O(log(n) base p)
		int pow = 0;
		for (long i = p; i <= n; i = i * p) {
			pow += n / i;
		}
		return pow;
	}

	private static boolean solve(int n, int m) {
		if (m == 0)
			return false;
		if (n >= m)
			return true;
		int u = 0;
		int k = m;
		int max = 0;
		for (int i = 0; i < a.size(); i++) {
			u = a.get(i);
			if (k < u)
				break;
			int num = 0;
			while (k % u == 0) {
				k /= u;
				num++;
			}
			if (num > 0)
				factors.add(new factor(u, num));
		}
		if (k > 1)
			if (n < k)
				return false;
			else
				factors.add(new factor(k, 1));
		for (int i = 0; i < factors.size(); i++)
			if (factors.get(i).num - getnum(n, factors.get(i).prime) > 0)
				return false;

		return true;
	}
}
