import java.util.Arrays;
import java.util.Scanner;

public class Pr10870 {
	static int d;
	static int N;
	static int M;
	static int[] coff;
	static long[] functions;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			d = in.nextInt();
			N = in.nextInt();
			M = in.nextInt();
			if (d == 0 && N == 0 && M == 0)
				break;
			coff = new int[d];
			functions = new long[d];
			Arrays.fill(functions, 0);
			for (int i = 0; i < d; i++)
				coff[i] = in.nextInt();
			for (int i = 0; i < d; i++)
				functions[i] = in.nextInt();
			for (int i = d ; i < N; i++)
				functions[i%d] = solve(i);
			System.out.println(functions[(N-1)%d]);
		}

	}

	private static long solve(int prev) {
		// TODO Auto-generated method stub
		long u = 0;
		int w = prev-d;
		for(int i = 0 ; i < d ;i++){
			u += coff[i]*functions[(prev-i-1)%d]%M; 
		}
		return u;
	}
}
