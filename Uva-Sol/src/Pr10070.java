import java.util.Scanner;
public class Pr10070 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while(in.hasNext()){
			long year = in.nextLong();
			boolean leap = false;
			boolean Huluculu = false;
			boolean bulukulu = false;
			boolean ordinary = true;
			if (year%4==0&&(year%100!=0||year%400==0)){
				ordinary = false;
				leap=true;
			}
			if (year%15==0){
				ordinary = false;
				Huluculu = true;
			}
			if (year%55==0){
				ordinary = false;
				leap=true;
				bulukulu = true;
			}
			if (ordinary){
				System.out.println("This is an ordinary year.");
			}else{
				if(leap){
					System.out.println("This is leap year.");
				}
				if(Huluculu){
					System.out.println("This is huluculu festival year.");
				}

				if(bulukulu){
					System.out.println("This is bulukulu festival year.");
				}
			}
			System.out.println();
		}
	}

}
